package se.itu.game.cave;

import java.util.List;
import java.util.ArrayList;
import se.itu.game.cave.Room.Direction;
import se.itu.game.cave.init.CaveInitializer;

public class Player {

  private static Player player;

  private List<Thing> inventory;
  private Room currentRoom;

  private Player(Room room) {
    inventory = new ArrayList<Thing>();
    currentRoom = room;

  }

  public static Player getInstance() {
    if (player == null) {
      player = new Player(CaveInitializer.getInstance().getFirstRoom());
    }
    return player;

  }



  public void takeThing(Thing thing) {
  }

  public void dropThing(Thing thing) {
  }

  public List<Thing> inventory() {
    throw new UnsupportedOperationException();
  }

  public Room currentRoom() {
    return currentRoom;
  }

  public void go(Direction direction) {
    throw new UnsupportedOperationException();
  }

  public String toString() {
    throw new UnsupportedOperationException();
  }


}
