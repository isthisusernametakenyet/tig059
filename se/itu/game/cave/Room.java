package se.itu.game.cave;

import se.itu.game.cave.Thing;

import java.util.*;

public class Room {

  private String description;
  private Room north;
  private Room east;
  private Room south;
  private Room west;
  private List<Thing> things;

  public static enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;
  }

  // private Direction direction; // enum { 0, 1, 2, 3 }
  public Room(String description,
          Room north,
          Room east,
          Room south,
          Room west,
          List<Thing> things) {
    if (description == null || things == null) {
      throw new NullPointerException("null!");
    }
    this.description = description;
    this.north = north;
    this.east = east;
    this.south = south;
    this.west = west;
    this.things = things;
  }

  public Room getRoom(Direction direction) {
    switch (direction) {
      case NORTH:
        return north;
      case EAST:
        return east;
      case SOUTH:
        return south;
      case WEST:
        return west;
      default:
        return null;
      //  return DEFAULT_VALUE;
    }
  }

  public void setConnectingRoom(Direction direction, Room room) {
    switch (direction) {
      case NORTH:
        north = room;
        break;
      case EAST:
        east = room;
        break;
      case SOUTH:
        south = room;
        break;
      case WEST:
        west = room;
        break;
      default:
        exit(1);
    }
  }

  public Thing removeThing(Thing thing) {
    if (thing == null) {
      throw new NullPointerException("null thing");
    }
    Thing foundThing = null;
    String tn = thing.name();
    for (int i = 0; i < things.size(); i++) {
      if (tn.equals(things.get(i).name()) == true) {
        foundThing = things.remove(i); // won't work
      }
    }
    return foundThing;
  }

  public void putThing(Thing t) {
    things.add(t); // won't work
  }

  public List<Thing> things() {
    return this.things;
  }

  public String description() {
    return this.description;
  }

  @Override
  public String toString() {
    return this.description + " " + this.things;
  }

  private void exit(int status) {
    System.exit(status);
  }

}
