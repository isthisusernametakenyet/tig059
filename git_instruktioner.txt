git status					Visar status i mappen

git add .					Förbereder samtliga filer i aktuell mapp för uppladdning till ny version

git commit -m "meddelande som "			Skickar filer som lagts till via add till nästa version

git log						Visar loggfil för aktuell mapp

git push					Skickar upp lokal mapp till extern mapp på internet

git pull					Hämtar senaste ändringar från extern mapp på internet
